package Package;

import org.testng.annotations.Test;



import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

//abc
public class NewTest {
FirefoxDriver fd;
static WebElement element;
@Test
public void TC001() {
int flag = 0;
element = fd.findElementByName("userName");
element.sendKeys("a.a@a.com");
element = fd.findElementByName("password");
element.sendKeys("12345");
element = fd.findElementByName("login");
element.click();
System.out.println("Running the main test");
element = fd.findElementByXPath("//a[contains(.,'SIGN-OFF')]");
if (element.isDisplayed()){
flag = 1;
}
Assert.assertEquals(flag, 1);
Reporter.log("Logged in Successfully");
}

@Test
public void TC002() throws InterruptedException {
int flag = 0;
element = fd.findElementByName("userName");
element.sendKeys("a.a@a.com");
element = fd.findElementByName("password");
element.sendKeys("12345");
element = fd.findElementByName("login");
element.click();
Thread.sleep(4000);

element = fd.findElementByXPath("//input[@value='roundtrip']");
if(element.isSelected())
{ element = fd.findElementByXPath("//input[@value='oneway']");
element.click();
System.out.println("Running The Main test");

if (element.isSelected()){
flag = 1;
}
Assert.assertEquals(flag, 1);
Reporter.log("Oneway selected Successfully");
}
}


@BeforeMethod
public void beforeMethod() {
fd.get("http://www.newtours.demoaut.com");
System.out.println("Running before method");
}

@AfterMethod
public void afterMethod() {
System.out.println("Running after method");
element = fd.findElementByXPath("//a[contains(.,'SIGN-OFF')]");
element.click();
}

@BeforeClass
public void beforeClass() {
System.out.println("Running before class");
}

@AfterClass
public void afterClass() {
System.out.println("Running after class");
}


@BeforeTest
public void beforeTest() {
fd = new FirefoxDriver();
System.out.println("Running before test");
}

@AfterTest
public void afterTest() {
System.out.println("Running after test");
fd.close();
}

@BeforeSuite
public void beforeSuite() {
System.out.println("Running before suite");
}

@AfterSuite
public void afterSuite() {
System.out.println("Running after suite");
}
}